package api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.apache.spark.sql.Dataset;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import javax.xml.crypto.Data;
import java.util.StringJoiner;

public class DatabaseService {

    final SparkSession sparkSession;
    final JavaSparkContext sparkContext;
    final Dataset<Row> jdbcDF;

    public DatabaseService() {
        this.sparkSession = SparkSession.builder()
                .appName("Reddit CC")
                .master("local[*]")
                .config("spark.driver.maxResultSize", "4g")
                .config("spark.cores.max", 12)
                .config("spark.default.parallelism", 12)
                .getOrCreate();

        this.sparkContext = new JavaSparkContext(sparkSession.sparkContext());

        jdbcDF = sparkSession.read().format("com.databricks.spark.csv")
                .option("delimiter", ",")
                .option("header", "true")
                .load("C:\\May2015.csv");
    }

    public String getCommentCountByAuthor() {
        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("body", "author").createOrReplaceTempView("bodyAuthorQuery");
        Dataset<Row> result = sparkSession.sql("SELECT COUNT(body) AS count_body FROM bodyAuthorQuery GROUP BY author");

        result.select("count_body").createOrReplaceTempView("countBodyView");

        // TODO: Fix getting the average from here
        Dataset<Row> average = sparkSession.sql("SELECT AVG(count_body) FROM countBodyView");
        return average.select().collectAsList().get(0).getList(0).toString();
    }

    public String getMostPopularSubreddits() {

        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("subreddit").createOrReplaceTempView("popularityQuery");

        Dataset<Row> result = sparkSession.sql("SELECT subreddit, COUNT(subreddit) AS subredditOccurence FROM popularityQuery GROUP BY subreddit ORDER BY subredditOccurence DESC LIMIT 5");

        Gson gson = new GsonBuilder().create();
        String jsonArray = gson.toJson(result.collectAsList());

        return jsonArray;
    }


    public String getLengthUpvoteCorelation() {

        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("body", "ups").createOrReplaceTempView("upvoteLengthQuery");

        Dataset<Row> result = sparkSession.sql("SELECT ups, LENGTH(body) AS lengthBody FROM upvoteLengthQuery");

        Gson gson = new GsonBuilder().create();
        String jsonArray = gson.toJson(result.collectAsList());

        return jsonArray;
    }

    public String getRemovedControversial() {

        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("body", "link_id", "removal_reason", "controversiality").createOrReplaceTempView("controversialQuery");

        Dataset<Row> resultControversial = sparkSession.sql("SELECT body, link_id FROM controversialQuery WHERE removal_reason IS NOT NULL AND controversiality = 1");
        Dataset<Row> resultRemoved = sparkSession.sql("SELECT body, link_id FROM controversialQuery WHERE removal_reason IS NOT NULL");

        Long removedControversialCount = resultControversial.count();
        Long totalRemovedCount = resultRemoved.count();

        Long percentageOfControversialRemoved = (removedControversialCount * 100) / totalRemovedCount;


        resultControversial.show();
        System.out.println("Percentage of controversial removed: " + percentageOfControversialRemoved);

        return  percentageOfControversialRemoved.toString();
    }

    public String getCommentsByAuthor(String nameToSearch) {

        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("body", "author").createOrReplaceTempView("authorQuery");

        String s = String.format("SELECT body FROM authorQuery WHERE author = '%s'", nameToSearch);
        Dataset<Row> result = sparkSession.sql(s);

        return result.toJSON().collectAsList().toString();
    }

    public String getCommentsBySubreddit(String subredditToSearch) {

        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("body", "subreddit").createOrReplaceTempView("subredditQuery");

        String s = String.format("SELECT body FROM subreddit Query WHERE subreddit = '%s'", subredditToSearch);
        Dataset<Row> result = sparkSession.sql(s);

        return result.toJSON().collectAsList().toString();
    }

    public String getCommentsByCreatedUtc(String createdUtcToSearch) {

        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("body", "created_utc").createOrReplaceTempView("createdUtcQuery");

        String s = String.format("SELECT body FROM created_utc Query WHERE created_utc = '%s'", createdUtcToSearch);
        Dataset<Row> result = sparkSession.sql(s);

        return result.toJSON().collectAsList().toString();
    }

    public String getCommentsByUpvotes(String upsToSearch) {

        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("body", "ups").createOrReplaceTempView("upsToSearchQuery");

        String s = String.format("SELECT body FROM ups Query WHERE ups = '%s'", upsToSearch);
        Dataset<Row> result = sparkSession.sql(s);

        return result.toJSON().collectAsList().toString();
    }

    public String getCommentHourDistribution(String dayToFind) {

        Dataset<Row> copyDF = jdbcDF;
        copyDF.select("created_utc").createOrReplaceTempView("createdQuery");

       // "SELECT from_unixtime(created_utc, 'yyyy-MM-dd HH:mm:ss') FROM createdQuery"

        String s = String.format("SELECT HOUR(from_unixtime(created_utc, 'yyyy-MM-dd HH:mm:ss')) AS hourBody FROM createdQuery WHERE DAY(from_unixtime(created_utc, 'yyyy-MM-dd HH:mm:ss')) = '%s'", dayToFind);
        Dataset<Row> result = sparkSession.sql(s);

        result.select("hourBody").createOrReplaceTempView("currentDayQuery");


        JsonObject toReturnJSON = new JsonObject();

        for (int i = 0; i < 24; ++i)
        {
            String toRun = String.format("SELECT hourBody FROM currentDayQuery WHERE hourBody = '%s'", i);
            Dataset<Row> currentResult = sparkSession.sql(toRun);

            toReturnJSON.addProperty("count", currentResult.count());
            toReturnJSON.addProperty("hour", i);
        }

        return toReturnJSON.getAsString();
    }

}

