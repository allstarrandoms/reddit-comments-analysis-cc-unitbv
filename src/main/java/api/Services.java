package api;

import com.google.gson.JsonObject;

import static spark.Spark.get;

public class Services {

    static private DatabaseService databaseService = new DatabaseService();

    public Services()
    {
        getMostPopularSubreddits();
        getCommentCountByAuthor();
        getSearchByAuthor();
        // getLengthUpvoteCorelation();
        getCommentsByUpvotes();
        getCommentsBySubreddit();
        getCommentHourDistribution();
        getCommentsByCreatedUtc();
    }

    public static void getMostPopularSubreddits(){
        get("/mostpopular",(req,res) -> {

            return databaseService.getMostPopularSubreddits();
        });
    }

    public static void getCommentCountByAuthor() {
        get("/commentcount",(req,res) -> {

            return databaseService.getCommentCountByAuthor();
        });
    }

    public static void getSearchByAuthor() {
        get( "/get/author/:name", (req, res) ->
        {
            return databaseService.getCommentsByAuthor(req.params(":name"));
        });
    }

    public static void getCommentsByUpvotes() {
        get( "/get/upvotes/:upvotes", (req, res) ->
        {
            return databaseService.getCommentsByUpvotes(req.params(":upvotes"));
        });
    }

    public static void getCommentsByCreatedUtc() {
        get( "/get/created/:utc", (req, res) ->
        {
            return databaseService.getCommentsByCreatedUtc(req.params(":utc"));
        });
    }
    public static void getCommentsBySubreddit() {
        get( "/get/subreddit/:subreddit", (req, res) ->
        {
            return databaseService.getCommentsBySubreddit(req.params(":subreddit"));
        });
    }

    public static void getLengthUpvoteCorelation() {
        get( "/lenghtupvote", (req, res) ->
        {
            return databaseService.getLengthUpvoteCorelation();
        });
    }

    public static void getCommentHourDistribution() {
        get( "/commentHour/:day", (req, res) ->
        {
            return databaseService.getCommentHourDistribution(req.params(":day"));
        });
    }

    public static void getRemovedControversialPercentage(){
        get("/get/removedControversial/:percentage", (req, res) -> {

            return databaseService.getRemovedControversial();
        });
    }



}
