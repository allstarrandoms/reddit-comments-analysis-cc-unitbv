import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import BasicForm from './BasicForm/BasicForm';

class App extends Component {

  state = {
    
  }

  getApiResponse = () =>{
    const api_call =  fetch('http://localhost:27011/Json');
    const data =  api_call.json;

    console.log(data);
  }


  render() {
    return (
      <>
     <h1>This is a message</h1>
     <h2>This is another message</h2>
     <button type="button" className="btn" onClick = {this.getApiResponse}>Basic</button>
     <BasicForm></BasicForm>
     </>
    );
  }
}

export default App;
