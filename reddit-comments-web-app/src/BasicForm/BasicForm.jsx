import React from 'react'
import { stringify } from 'querystring';


class BasicForm extends React.Component{

    state={
        array : [],
        formValue : 423

    }

    onChanged = (e) => {
        this.setState({formValue: e.currentTarget.value})
        console.log("changed" + this.state.formValue);
    }

    // type class classname  state props key

    render(){
        return(
            <div className="form-group">
            <label for="usr">Name:</label>
            <input type="number" className="form-control" onChange = {this.onChanged} />
            </div>
        )   
    }

}

export default BasicForm;
